#!/usr/bin/env python

"""
    Control the joints of a robot using a skeleton tracker such as the
    OpenNI tracker package in junction with a Kinect RGB-D camera.
    
    Based on Taylor Veltrop's C++ work (http://www.ros.org/wiki/veltrop-ros-pkg)
    
    Created for the Pi Robot Project: http://www.pirobot.org
    Copyright (c) 2011 Patrick Goebel.  All rights reserved.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details at:
    
    http://www.gnu.org/licenses/gpl.html
"""

import rospy
import pi_tracker_lib as PTL
import zeno_tracker_lib as ZTL
from sensor_msgs.msg import JointState
from skeleton_markers.msg import Skeleton
from std_msgs.msg import Float64
from std_msgs.msg import String
from zeno_tracker.srv import *
import PyKDL as KDL
from math import acos, asin, pi, sin, cos, atan, atan2

from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint


class TrackerJointController():
    def __init__(self):
        rospy.init_node('tracker_joint_controller')
        rospy.on_shutdown(self.shutdown)
        
        rospy.loginfo("Initializing Joint Controller Node...")
        
        self.rate = rospy.get_param('~joint_controller_rate', 5.0)
        
        rate = rospy.Rate(self.rate)
        
        # The namespace may be set by the servo controller (usually null)
        namespace = rospy.get_namespace()
        
        self.joints = rospy.get_param(namespace + '/joints', '')
        
        self.skel_to_joint_map = rospy.get_param("~skel_to_joint_map", dict())
        
        self.default_joint_speed = rospy.get_param('~default_joint_speed', 0.5)
        
        self.tracker_commands = rospy.get_param('~tracker_commands', ['e_start', 'e_stop'])
         
        self.HALF_PI = pi / 2.0

        self.activate_cmd_sub = rospy.Subscriber("~event_in", String, self.activate_cmd_cb)

        self.previous_joint_positions = []


        # Subscribe to the skeleton topic.
        rospy.Subscriber('skeleton', Skeleton, self.skeleton_handler)
        
        # Store the current skeleton configuration in a local dictionary.
        self.skeleton = dict()
        self.skeleton['confidence'] = dict()
        self.skeleton['position'] = dict()
        self.skeleton['orientation'] = dict()
         
        # Set up the tracker command service for this node.
        self.set_command = rospy.Service('~set_command', SetCommand, self.set_command_callback)

        self.zeno_position_trajectoy_cmd_pub =  rospy.Publisher("/body_controller/trajectory_command", JointTrajectory, queue_size=1)
        #self.eventin_sub = rospy.Subscriber("~event_in", String, eventin_cb)
        # The get_joints command parses a URDF description of the robot to get all the non-fixed joints.
        self.cmd_joints = PTL.get_joints()
        
        # Store the last joint command so we can stop and hold a given posture.
        self.last_cmd_joints = PTL.get_joints()
        
        # Initialize the robot in the stopped state.
        self.tracker_command = "e_stop"

        self.activate_cmd = "e_stop"

        rospy.loginfo('Skeleton tracker is started.')
        rospy.loginfo('Now calibrate skeleton tracker.')
        rospy.loginfo('Make a pi shape with the arms infront of the kinect untill tracker is calibrated.')

        while not rospy.is_shutdown():              
            # Execute the behavior appropriate for the current command.
            if self.activate_cmd in self.tracker_commands:
                if self.activate_cmd == 'e_stop':
                    self.stop_joints()
                else:
                    self.teleop_joints()
                    #self.teleop_joints_lb()
                
                self.cmd_joints.header.stamp = rospy.Time.now()
                #self.joint_state = self.cmd_joints
                #self.joint_state_pub.publish(self.joint_state)
            else:
                pass
                
            self.last_cmd_joints = self.cmd_joints
            
            rate.sleep()          

    def stop_joints(self):
        self.cmd_joints = self.last_cmd_joints


    def teleop_joints_lb(self):

        '''
        publishTransform(kinect_controller, user, XN_SKEL_HEAD,           fixed_frame, "head", g_skel);
        publishTransform(kinect_controller, user, XN_SKEL_NECK,           fixed_frame, "neck", g_skel);
        publishTransform(kinect_controller, user, XN_SKEL_TORSO,          fixed_frame, "torso", g_skel);

        publishTransform(kinect_controller, user, XN_SKEL_LEFT_SHOULDER,  fixed_frame, "left_shoulder", g_skel);
        publishTransform(kinect_controller, user, XN_SKEL_LEFT_ELBOW,     fixed_frame, "left_elbow", g_skel);
        publishTransform(kinect_controller, user, XN_SKEL_LEFT_HAND,      fixed_frame, "left_hand", g_skel);

        publishTransform(kinect_controller, user, XN_SKEL_RIGHT_SHOULDER, fixed_frame, "right_shoulder", g_skel);
        publishTransform(kinect_controller, user, XN_SKEL_RIGHT_ELBOW,    fixed_frame, "right_elbow", g_skel);
        publishTransform(kinect_controller, user, XN_SKEL_RIGHT_HAND,     fixed_frame, "right_hand", g_skel);

        publishTransform(kinect_controller, user, XN_SKEL_LEFT_HIP,       fixed_frame, "left_hip", g_skel);
        publishTransform(kinect_controller, user, XN_SKEL_LEFT_KNEE,      fixed_frame, "left_knee", g_skel);
        publishTransform(kinect_controller, user, XN_SKEL_LEFT_FOOT,      fixed_frame, "left_foot", g_skel);

        publishTransform(kinect_controller, user, XN_SKEL_RIGHT_HIP,      fixed_frame, "right_hip", g_skel);
        publishTransform(kinect_controller, user, XN_SKEL_RIGHT_KNEE,     fixed_frame, "right_knee", g_skel);
        publishTransform(kinect_controller, user, XN_SKEL_RIGHT_FOOT,     fixed_frame, "right_foot", g_skel);
        '''
        # Left Leg
        try:
            left_hip_knee = self.skeleton['position']['left_knee'] - self.skeleton['position']['left_hip']
            left_knee_foot= self.skeleton['position']['left_foot'] - self.skeleton['position']['left_knee']
            left_hip_foot= self.skeleton['position']['left_foot'] - self.skeleton['position']['left_hip']

            left_hip_knee.Normalize()
            left_knee_foot.Normalize()
            left_hip_foot.Normalize()                           

            #right_hip_pitch = -asin(left_hip_knee.z())

            default_hip_knee = KDL.Vector(0,-1,0)
            right_hip_pitch = acos(KDL.dot(default_hip_knee,left_hip_knee)/(default_hip_knee.Norm() * left_hip_knee.Norm()))
            ni_x_nc = (default_hip_knee * left_hip_knee)
            #print "ni_x_nc",ni_x_nc

            if ni_x_nc.x() < 0.0:
               right_hip_pitch =  -right_hip_pitch

            right_hip_roll = -asin(left_hip_knee.x())

            #print 'right_hip_pitch', right_hip_pitch

            #TODO: right hip yaw angle 
            '''

            #right_hip_yaw = atan2(-left_hip_knee.x(), -left_hip_knee.z())
            #or 
            nc = left_shoulder_elbow *  left_arm_elbow_flex_hand

            alpha = left_arm_shoulder_lift_angle
            beta = -left_arm_shoulder_pan_angle
             
            sin_alpha = sin(alpha)
            cos_alpha = cos(alpha)

            sin_beta = sin(beta)
            cos_beta = cos(beta)
            

            R_alpha_beta = KDL.Rotation(cos_beta,-sin_beta,0,
                             cos_alpha*sin_beta, cos_alpha*cos_beta, -sin_alpha,
                             sin_alpha*sin_beta, sin_alpha*cos_beta, cos_alpha 
                            )

            default_surface_normal = KDL.Vector(0,0,1)

            ni = R_alpha_beta * default_surface_normal

            elbow_roll_magnitude = acos(KDL.dot(ni,nc)/(ni.Norm() * nc.Norm()))

            ni_x_nc = (ni * nc)

            if ni_x_nc.x() >= 0.0:
               left_elbow_roll_angle =  elbow_roll_magnitude
            else:
               left_elbow_roll_angle =  -elbow_roll_magnitude
            '''

            #right knee pitch angle
            right_knee_pitch = acos(KDL.dot(left_hip_knee,left_knee_foot)/(left_hip_knee.Norm() * left_knee_foot.Norm()))
            #print 'RIGHT[hip_pitch,hip_roll,knee_pitch]',right_hip_pitch, right_hip_roll,right_knee_pitch
        except KeyError:
            pass
            
        # Right Leg
        try:
            right_hip_knee = self.skeleton['position']['right_knee'] - self.skeleton['position']['right_hip']
            right_knee_foot= self.skeleton['position']['right_foot'] - self.skeleton['position']['right_knee']
            right_hip_foot= self.skeleton['position']['right_foot'] - self.skeleton['position']['right_hip']

            right_hip_knee.Normalize()
            right_knee_foot.Normalize()
            right_hip_foot.Normalize()                           

            left_hip_pitch = -asin(right_hip_knee.z())         
            left_hip_roll = -asin(right_hip_knee.x())


            #TODO: right hip yaw angle
            '''
            nc = right_shoulder_elbow *  right_arm_elbow_flex_hand

            alpha = right_arm_shoulder_lift_angle
            beta = -right_arm_shoulder_pan_angle
             
            sin_alpha = sin(alpha)
            cos_alpha = cos(alpha)

            sin_beta = sin(beta)
            cos_beta = cos(beta)
            

            R_alpha_beta = KDL.Rotation(cos_beta,-sin_beta,0,
                             cos_alpha*sin_beta, cos_alpha*cos_beta, -sin_alpha,
                             sin_alpha*sin_beta, sin_alpha*cos_beta, cos_alpha 
                            )

            default_surface_normal = KDL.Vector(0,0,1)

            ni = R_alpha_beta * default_surface_normal

            elbow_roll_magnitude = acos(KDL.dot(ni,nc)/(ni.Norm() * nc.Norm()))

            ni_x_nc = (ni * nc)

            if ni_x_nc.x() >= 0.0:
               right_elbow_roll_angle =  elbow_roll_magnitude
            else:
               right_elbow_roll_angle =  -elbow_roll_magnitude
            '''

            #right knee pitch angle
            left_knee_pitch = acos(KDL.dot(right_hip_knee,right_knee_foot)/(right_hip_knee.Norm() * right_knee_foot.Norm()))

            #print 'LEFT[hip_pitch,hip_roll,knee_pitch]',left_hip_pitch, left_hip_roll,left_knee_pitch
    
        except KeyError:
            pass
            
    def teleop_joints(self):
        # Head Pan/neck yaw
        '''       
        try:
            head_quaternion = self.skeleton['orientation']['head']
            head_rpy = head_quaternion.GetRPY()
            self.cmd_joints.position[self.cmd_joints.name.index('neck_yaw_joint')] = head_rpy[1]
            self.cmd_joints.velocity[self.cmd_joints.name.index('neck_yaw_joint')] = self.default_joint_speed
        except:
            pass
        ''' 
        # Head Tilt/neck pitch
        ''' 
        try:
            self.cmd_joints.position[self.cmd_joints.name.index('neck_pitch_joint')] = -head_rpy[0]
            self.cmd_joints.velocity[self.cmd_joints.name.index('neck_pitch_joint')] = self.default_joint_speed
        except:
            pass
        ''' 
        # Head Zoom/neck roll
        ''' 
        try:
            self.cmd_joints.position[self.cmd_joints.name.index('neck_roll_joint')] = head_rpy[2]
            self.cmd_joints.velocity[self.cmd_joints.name.index('neck_roll_joint')] = self.default_joint_speed
        except:
            pass
        ''' 
        # Left Arm
        try:
            left_shoulder_neck = self.skeleton['position']['neck'] - self.skeleton['position']['left_shoulder']
            left_shoulder_elbow = self.skeleton['position']['left_elbow'] - self.skeleton['position']['left_shoulder']
            left_arm_elbow_flex_hand = self.skeleton['position']['left_hand'] - self.skeleton['position']['left_elbow']
            left_shoulder_hand = self.skeleton['position']['left_hand'] - self.skeleton['position']['left_shoulder']
            left_shoulder_position = self.skeleton['position']['left_shoulder']

            left_shoulder_neck.Normalize()
            left_shoulder_elbow.Normalize()
            lh = left_arm_elbow_flex_hand.Normalize()
            left_shoulder_hand.Normalize()

            '''
            compute left shouler pitch angle
            '''
            v_temp = KDL.Vector(left_shoulder_elbow.x() , left_shoulder_elbow.y() ,left_shoulder_elbow.z())

            R_temp = KDL.Rotation(1,0,0,
                 0,  cos(pi), - sin(pi),
                 0, sin(pi), cos(pi) 
                )

            v_temp = R_temp*v_temp

            left_arm_shoulder_lift_angle_temp = -1*atan2(v_temp.y(), v_temp.z())

            left_arm_shoulder_lift_angle = asin(left_shoulder_elbow.y()) + self.HALF_PI
            left_arm_shoulder_pan_angle = asin(left_shoulder_elbow.x())         #left_arm_elbow_flex_hand
            left_arm_elbow_flex_angle = acos(KDL.dot(left_shoulder_elbow, left_arm_elbow_flex_hand))
            left_arm_wrist_flex_angle = -left_arm_elbow_flex_angle

            
            '''
            zeno's right elbow yaw angle
            '''
            nc = left_shoulder_elbow *  left_arm_elbow_flex_hand

            alpha = left_arm_shoulder_lift_angle
            beta = -left_arm_shoulder_pan_angle
             
            sin_alpha = sin(alpha)
            cos_alpha = cos(alpha)

            sin_beta = sin(beta)
            cos_beta = cos(beta)
            

            R_alpha_beta = KDL.Rotation(cos_beta,-sin_beta,0,
                             cos_alpha*sin_beta, cos_alpha*cos_beta, -sin_alpha,
                             sin_alpha*sin_beta, sin_alpha*cos_beta, cos_alpha 
                            )

            default_surface_normal = KDL.Vector(0,0,1)

            ni = R_alpha_beta * default_surface_normal

            elbow_roll_magnitude = acos(KDL.dot(ni,nc)/(ni.Norm() * nc.Norm()))

            ni_x_nc = (ni * nc)
            
            if ni_x_nc.x() >= 0.0:
               left_elbow_roll_angle =  elbow_roll_magnitude
            else:
               left_elbow_roll_angle =  -elbow_roll_magnitude

            #print "=============right_roll_angle============"

            left_elbow_roll_angle = (left_elbow_roll_angle - self.HALF_PI)

            '''
            Using Zeno tracker Normalizer
            '''
            self.cmd_joints.position[self.cmd_joints.name.index('right_shoulder_pitch')] = left_arm_shoulder_lift_angle_temp #(left_arm_shoulder_lift_angle - self.HALF_PI)
            self.cmd_joints.velocity[self.cmd_joints.name.index('right_shoulder_pitch')] = self.default_joint_speed
            
            self.cmd_joints.position[self.cmd_joints.name.index('right_shoulder_roll')] = left_arm_shoulder_pan_angle + self.HALF_PI
            self.cmd_joints.velocity[self.cmd_joints.name.index('right_shoulder_roll')] = self.default_joint_speed
            
            self.cmd_joints.position[self.cmd_joints.name.index('right_elbow_pitch')] = left_arm_elbow_flex_angle
            self.cmd_joints.velocity[self.cmd_joints.name.index('right_elbow_pitch')] = self.default_joint_speed                                   
        
            self.cmd_joints.position[self.cmd_joints.name.index('right_wrist_yaw')] = left_arm_wrist_flex_angle
            self.cmd_joints.velocity[self.cmd_joints.name.index('right_wrist_yaw')] = self.default_joint_speed
            
            left_arm_shoulder_roll_angle = acos(KDL.dot(left_shoulder_elbow, left_shoulder_neck))
            self.cmd_joints.position[self.cmd_joints.name.index('right_elbow_yaw')] = left_elbow_roll_angle
            self.cmd_joints.velocity[self.cmd_joints.name.index('right_elbow_yaw')] = self.default_joint_speed
    
        except KeyError:
            pass  
            
        # Right Arm
        try:
            right_shoulder_neck = self.skeleton['position']['neck'] - self.skeleton['position']['right_shoulder']
            right_shoulder_elbow = self.skeleton['position']['right_elbow'] - self.skeleton['position']['right_shoulder']
            right_arm_elbow_flex_hand = self.skeleton['position']['right_hand'] - self.skeleton['position']['right_elbow']
            right_shoulder_hand = self.skeleton['position']['left_hand'] - self.skeleton['position']['left_shoulder']

            right_shoulder_position = self.skeleton['position']['right_shoulder']
            
            right_shoulder_neck.Normalize()
            right_shoulder_elbow.Normalize()
            rh = right_arm_elbow_flex_hand.Normalize()
            right_shoulder_hand.Normalize()

            v_temp = KDL.Vector(right_shoulder_elbow.x() , right_shoulder_elbow.y() ,right_shoulder_elbow.z())

            R_temp = KDL.Rotation(1,0,0,
                 0,  cos(pi), - sin(pi),
                 0, sin(pi), cos(pi) 
                )

            v_temp = R_temp*v_temp

            right_arm_shoulder_lift_angle_temp = atan2(v_temp.y(), v_temp.z())

            #print 'right_shoulder_elbow.y()', right_shoulder_elbow.y()
            right_arm_shoulder_lift_angle = (asin(right_shoulder_elbow.y()) + self.HALF_PI)
            right_arm_shoulder_pan_angle = asin(right_shoulder_elbow.x())
            right_arm_elbow_flex_angle = acos(KDL.dot(right_shoulder_elbow, right_arm_elbow_flex_hand))
            right_arm_wrist_flex_angle = right_arm_elbow_flex_angle

            '''
            zeno right elbow yaw angle
            '''
            nc = (right_shoulder_elbow *  right_arm_elbow_flex_hand)

            alpha = right_arm_shoulder_lift_angle
            beta = -right_arm_shoulder_pan_angle
             
            sin_alpha = sin(alpha)
            cos_alpha = cos(alpha)

            sin_beta = sin(beta)
            cos_beta = cos(beta)
            

            R_alpha_beta = KDL.Rotation(cos_beta,-sin_beta,0,
                             cos_alpha*sin_beta, cos_alpha*cos_beta, -sin_alpha,
                             sin_alpha*sin_beta, sin_alpha*cos_beta, cos_alpha 
                            )

            default_surface_normal = KDL.Vector(0,0,1)

            ni = R_alpha_beta * default_surface_normal

            elbow_roll_magnitude = acos(KDL.dot(ni,nc)/(ni.Norm() * nc.Norm()))

            ni_x_nc = (ni * nc)

            if ni_x_nc.x() > 0.0:
               right_elbow_roll_angle =  -elbow_roll_magnitude
            else:
               right_elbow_roll_angle =  elbow_roll_magnitude


            '''
            zeno torso angle 
            '''
            
            right_elbow_roll_angle = right_elbow_roll_angle - self.HALF_PI

            z_mean = (right_shoulder_position.z() + left_shoulder_position.z())/2
            x_mean = (right_shoulder_position.x() + left_shoulder_position.x())/2

            delta_z_LS = z_mean - left_shoulder_position.z()
            delta_x_LS = x_mean - left_shoulder_position.x()

            angle_LS_wrt_mean_point =  atan(delta_z_LS/delta_x_LS)

            delta_z_RS = z_mean - right_shoulder_position.z()
            delta_x_RS = x_mean - right_shoulder_position.x()

            angle_RS_wrt_mean_point =  atan(delta_z_RS/delta_x_RS)

            torso_angle = (angle_LS_wrt_mean_point + angle_RS_wrt_mean_point)/2
  

            self.cmd_joints.position[self.cmd_joints.name.index('left_shoulder_pitch')] = right_arm_shoulder_lift_angle_temp #self.HALF_PI - right_arm_shoulder_lift_angle
            self.cmd_joints.velocity[self.cmd_joints.name.index('left_shoulder_pitch')] = self.default_joint_speed
            
            self.cmd_joints.position[self.cmd_joints.name.index('left_shoulder_roll')] = right_arm_shoulder_pan_angle - self.HALF_PI
            self.cmd_joints.velocity[self.cmd_joints.name.index('left_shoulder_roll')] = self.default_joint_speed
            
            self.cmd_joints.position[self.cmd_joints.name.index('left_elbow_pitch')] = right_arm_elbow_flex_angle
            self.cmd_joints.velocity[self.cmd_joints.name.index('left_elbow_pitch')] = self.default_joint_speed
            
            self.cmd_joints.position[self.cmd_joints.name.index('left_wrist_yaw')] = right_arm_wrist_flex_angle
            self.cmd_joints.velocity[self.cmd_joints.name.index('left_wrist_yaw')] = self.default_joint_speed
 
            right_arm_shoulder_roll_angle = -asin(right_shoulder_hand.x())
            self.cmd_joints.position[self.cmd_joints.name.index('left_elbow_yaw')] =   right_elbow_roll_angle
            self.cmd_joints.velocity[self.cmd_joints.name.index('left_elbow_yaw')] = self.default_joint_speed

            self.cmd_joints.position[self.cmd_joints.name.index('torso')] = torso_angle
            self.cmd_joints.velocity[self.cmd_joints.name.index('torso')] = self.default_joint_speed

            self.publish_joint_cmd()

        except KeyError:
            pass
            #print 'skeleton tracker is not calibrated.'


    def publish_joint_cmd(self):
        joint_position_limit_violation = True

        joints_names = []
        joint_positions = []

        for joint_name in ZTL.joint_names:
            position = self.cmd_joints.position[self.cmd_joints.name.index(joint_name)]
            if (position > ZTL.joint_limit_min[joint_name] and position < ZTL.joint_limit_max[joint_name]):
                joints_names.append(joint_name)
                joint_positions.append(position)
            elif (position <= ZTL.joint_limit_min[joint_name]):
                joints_names.append(joint_name)
                joint_positions.append(ZTL.joint_limit_min[joint_name])
            elif (position >= ZTL.joint_limit_max[joint_name]):
                joints_names.append(joint_name)
                joint_positions.append(ZTL.joint_limit_max[joint_name])
            else:
                position = self.last_cmd_joints.position[self.last_cmd_joints.name.index(joint_name)]

        if (len(joint_positions) != 0):
            self.publish_zeno_trajectory_command(joints_names, joint_positions)

    def filter_trajectory(self, joint_positions):
        ALPHA = 0.25
        if len(self.previous_joint_positions) == 0 or \
            not(len(self.previous_joint_positions) == len(joint_positions)):
            self.previous_joint_positions = joint_positions

        for idx, joint_position in enumerate(joint_positions):
            #joint_positions[idx] = (self.previous_joint_positions[idx] * ALPHA) + (joint_position*(1.0 - ALPHA)) 
            joint_positions[idx] = (self.previous_joint_positions[idx] * (1-ALPHA)) + (joint_position*ALPHA)

        self.previous_joint_positions = joint_positions
        return joint_positions

    def publish_zeno_trajectory_command(self,joint_names, joint_positions):
        #joint_positions = self.filter_trajectory(joint_positions)

        eventout_msg = JointTrajectory()
        points = JointTrajectoryPoint()
        eventout_msg.joint_names = joint_names
        points.positions = joint_positions
        eventout_msg.points.append(points)
        self.zeno_position_trajectoy_cmd_pub.publish(eventout_msg)

    def pub_joint_cmds(self, cmd_joints):
        # First set the joint speeds
        for joint in sorted(self.joints):
            self.servo_speed[joint](cmd_joints.velocity[self.cmd_joints.name.index(joint)])
            
        # Then set the joint positions
        for joint in sorted(self.joints):
            self.servo_position[joint].publish(cmd_joints.position[self.cmd_joints.name.index(joint)])
            
    def skeleton_handler(self, msg):
        for joint in msg.name:  
            self.skeleton['confidence'][joint] = msg.confidence[msg.name.index(joint)]
            self.skeleton['position'][joint] = KDL.Vector(msg.position[msg.name.index(joint)].x, msg.position[msg.name.index(joint)].y, msg.position[msg.name.index(joint)].z)
            self.skeleton['orientation'][joint] = KDL.Rotation.Quaternion(msg.orientation[msg.name.index(joint)].x, msg.orientation[msg.name.index(joint)].y, msg.orientation[msg.name.index(joint)].z, msg.orientation[msg.name.index(joint)].w)
            
    def joint_state_handler(self, msg):
        for joint in msg.name:  
            self.joint_state = msg
            
    def set_command_callback(self, req):
        self.tracker_command = req.command
        return SetCommandResponse()

    def shutdown(self):
        rospy.loginfo('Shutting down Tracker Joint Controller Node.')

    def activate_cmd_cb(self, msg):
        self.activate_cmd = msg.data
        
if __name__ == '__main__':
    try:
        TrackerJointController()
    except rospy.ROSInterruptException:
        pass
