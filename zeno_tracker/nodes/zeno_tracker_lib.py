#!/usr/bin/env python

import rospy


    
def get_normalized_value(joint_name,joint_value):
    
    return functions[joint_name](joint_value,joint_limits[joint_name])

def right_arm_shoulder_lift(joint_value,joint_limit):
    joint_value = (joint_value)*(1/joint_limit)
    return joint_value

def right_arm_shoulder_pan(joint_value,joint_limit):
    if(joint_value < 0.0):
        joint_value = (joint_value)*(1/joint_limit)
    else:
        joint_value = 0.0
        
    return joint_value

def right_arm_elbow_flex(joint_value,joint_limit):
    if(joint_value > 0.0):
        joint_value = (joint_value)*(1/joint_limit)
    else:
        joint_value = 0.0

    return joint_value

def left_arm_shoulder_lift(joint_value,joint_limit):
    joint_value = (joint_value)*(1/joint_limit)
    return joint_value

def left_arm_shoulder_pan(joint_value,joint_limit):
    if(joint_value < 0.0):
        joint_value = (joint_value)*(1/joint_limit)
    else:
        joint_value = 0.0

    return joint_value

def left_arm_elbow_flex(joint_value,joint_limit):
    if(joint_value < 0.0):
        joint_value = (joint_value)*(1/joint_limit)
    else:
        joint_value = 0.0

    return joint_value

def normalize_joint_commands(joint_commands):
    joint_commands = list(joint_commands)
    joint_commands_index = 0
    for joint_name in joint_names:
        joint_commands[joint_commands_index] = (joint_commands[joint_commands_index]-joint_limit_min[joint_name])/(joint_limit_max[joint_name] - joint_limit_min[joint_name])
        joint_commands_index = joint_commands_index + 1

    return joint_commands

def normalize_joint_commands_test(names, joint_commands):
    #joint_commands = list(joint_commands)
    joint_commands_index = 0
    for joint_name in names:
        joint_commands[joint_commands_index] = (joint_commands[joint_commands_index]-joint_limit_min[joint_name])/(joint_limit_max[joint_name] - joint_limit_min[joint_name])
        joint_commands_index = joint_commands_index + 1

    return joint_commands

functions = {'right_arm_shoulder_lift': right_arm_shoulder_lift,
            'right_arm_shoulder_pan': right_arm_shoulder_pan, 
            'right_arm_elbow_flex': right_arm_elbow_flex,
            'left_arm_shoulder_lift': left_arm_shoulder_lift,
            'left_arm_shoulder_pan': left_arm_shoulder_pan, 
            'left_arm_elbow_flex': left_arm_elbow_flex,
            }
'''
    The values are taken from the urdf of the robot arm
'''
joint_limits = {'right_arm_shoulder_lift': -2.617,
            'right_arm_shoulder_pan': -1.8, 
            'right_arm_elbow_flex': 0.50,
            'left_arm_shoulder_lift': 2.617,
            'left_arm_shoulder_pan': 1.8, 
            'left_arm_elbow_flex': -0.50,
            }


joint_names = ['left_shoulder_pitch','left_shoulder_roll','left_elbow_pitch','left_elbow_yaw',
                'right_shoulder_pitch','right_shoulder_roll','right_elbow_pitch','right_elbow_yaw']

joint_names_zeno = ['torso','left_shoulder_pitch','left_shoulder_roll','left_elbow_pitch','left_wrist_yaw','left_elbow_yaw',
                'right_shoulder_pitch','right_shoulder_roll','right_elbow_pitch','right_wrist_yaw','right_elbow_yaw'
              ]

'''
joint_limit_min = {
                    'left_shoulder_pitch': -1.0123,
                    'left_shoulder_roll': 0.2,
                    'left_elbow_yaw': 0.2,
                    'left_elbow_pitch': 0.2,
                    'left_wrist_yaw': -1.5707,
                    'right_shoulder_pitch': -1.0123,
                    'right_shoulder_roll': 0.2,
                    'right_elbow_yaw': 0.2,
                    'right_elbow_pitch': 0.2,
                    'right_wrist_yaw': -1.5707}

joint_limit_max = {'torso': 0.785,
                    'left_shoulder_pitch': 3.5604,
                    'left_shoulder_roll': 1.2043,
                    'left_elbow_yaw': 3.1416,
                    'left_elbow_pitch': 1.3614,
                    'left_wrist_yaw': 1.5707,
                    'right_shoulder_pitch': 3.5604,
                    'right_shoulder_roll': 1.2043,
                    'right_elbow_yaw': 3.1416,
                    'right_elbow_pitch': 1.3614,
                    'right_wrist_yaw': 1.5707}
'''

joint_limit_min = { 
                    'torso': -1.02974426,
                    'left_shoulder_pitch': -0.5, #-2.05948852,
                    'left_shoulder_roll': -1.57,
                    'left_elbow_yaw': -1.57079633,
                    'left_elbow_pitch': 0.13962634,
                    'left_wrist_yaw': -1.5707,
                    'right_shoulder_pitch': -2.0, #-2.51327412,
                    'right_shoulder_roll': -0.12217305,
                    'right_elbow_yaw': -1.57079633,
                    'right_elbow_pitch': 0.13962634,
                    'right_wrist_yaw': -1.5707}

joint_limit_max = {'torso': 1.1693706,
                    'left_shoulder_pitch': 2.0, #2.51327412,
                    'left_shoulder_roll': 0.12217305,
                    'left_elbow_yaw': 1.57079633,
                    'left_elbow_pitch': 1.55334303,
                    'left_wrist_yaw': 1.5707,
                    'right_shoulder_pitch': 0.5, #1.95476876,
                    'right_shoulder_roll': 1.57,
                    'right_elbow_yaw': 1.57079633,
                    'right_elbow_pitch': 1.55334303,
                    'right_wrist_yaw': 1.5707}
