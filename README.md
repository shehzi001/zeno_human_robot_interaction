## Pre-requisite installations:
  * Robot Operating System (ROS)

  * Install dependencies using provided repository debs 

        ./repository.debs

  * Install NiTE v1.5.2.23 openni-sdk

        rosrun openni_tracker openni_tracker

        if you get the following error:

        [ERROR] [1403739716.958417396]: Find user generator failed: This operation is invalid!

        then download and install NiTE v1.5.2.23

        http://www.openni.ru/openni-sdk/openni-sdk-history-2/

        extract and install 
        sudo ./install.sh

        Run openni_tracker again. Now everything should be good.

  * Microsoft xbox 360 kinect 

## Skeleton tracker Usage:

Step 1: Start ROS master.
    
    roscore

### To use only skeleton tracking of the body except head tracking

Step 2: Run skeleton tracker

    roslaunch zeno_tracker skeleton_tracker.launch

Step 3: Calibrate skeleton tracker 
     
    Put your arms straight upward in u-shape infront of the kinect untill tracker window shows a skeleton.

Step 4: Start/Stop skeleton tracking component after calibration
     
    To start:
    rostopic pub /tracker_joint_controller/event_in std_msgs/String "data: 'e_start'"

    To stop:
    rostopic pub /tracker_joint_controller/event_in std_msgs/String "data: 'e_stop'"


### To use only head tracking

Step 5: run head pose estimation component

    roslaunch head_pose_estimation head_pose_estimation.launch

    or if glut window is required for debugging.

    roslaunch head_pose_estimation head_pose_estimation.launch debug:=true

Step 6: Start/Stop head tracker component.

    To start:
    rostopic pub /tracker_joint_controller/event_in std_msgs/String "data: 'e_start'"

    To stop:
    rostopic pub /tracker_joint_controller/event_in std_msgs/String "data: 'e_stop'"

### To run full body tracking and head tracking
  
Step 7: launch zeno skeleton body tracker + head tracker

    roslaunch zeno_skeleton_tracking skeleton_tracking.launch

Step 8: launch zeno skeleton tracker rqt ui to start and stop tracking components.

    roslaunch zeno_skeleton_tracker_rqt zeno_skeleton_tracker_rqt.launch
