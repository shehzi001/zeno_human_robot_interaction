cmake_minimum_required(VERSION 2.8.3)
project(zeno_skeleton_tracker_rqt)

find_package(catkin REQUIRED COMPONENTS
  rospy
  rqt_gui
  rqt_gui_cpp
  rqt_gui_py
)


find_package(Qt4 COMPONENTS QtCore QtGui REQUIRED)
include(${QT_USE_FILE})


catkin_package(
  LIBRARIES ${PROJECT_NAME}
  CATKIN_DEPENDS rqt_gui rqt_gui_cpp
)


catkin_python_setup()

include_directories(
  ${catkin_INCLUDE_DIRS}
)

install(FILES plugin.xml
  DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
)

install(PROGRAMS scripts/zeno_skeleton_tracker_rqt
  DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

