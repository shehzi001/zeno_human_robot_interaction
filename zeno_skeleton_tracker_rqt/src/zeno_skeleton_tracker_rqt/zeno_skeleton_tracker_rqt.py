import os
import rospy

import roslib; 
import rospy

from qt_gui.plugin import Plugin
from python_qt_binding import loadUi
from python_qt_binding.QtGui import QWidget

from PyQt4.Qt import QListView, QStandardItemModel, QStandardItem


from std_msgs.msg import String, Bool

class MyPlugin(Plugin):

    client = None

    def __init__(self, context):
        super(MyPlugin, self).__init__(context)
        # Give QObjects reasonable names
        self.setObjectName('ZENOSkeletonTrackerUIPlugin')

        # Process standalone plugin command-line arguments
        from argparse import ArgumentParser
        parser = ArgumentParser()
        # Add argument(s) to the parser.
        parser.add_argument("-q", "--quiet", action="store_true",
                      dest="quiet",
                      help="Put plugin in silent mode")
        args, unknowns = parser.parse_known_args(context.argv())
        '''
        if not args.quiet:
            print 'arguments: ', args
            print 'unknowns: ', unknowns
        '''

        # Create QWidget
        self._widget = QWidget()
        # Get path to UI file which is a sibling of this file
        # in this example the .ui and .py file are in the same folder
        ui_file = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'zeno_skeleton_tracker_rqt.ui')
        # Extend the widget with all attributes and children from UI file
        loadUi(ui_file, self._widget)
        # Give QObjects reasonable names

        self._widget.btnStartSkeletonTracking.clicked.connect(self.start_skeleton_tracking)
        self._widget.btnStopSkeletonTracking.clicked.connect(self.stop_skeleton_tracking)

        self._widget.btnStartBodyTracking.clicked.connect(self.start_body_tracking)
        self._widget.btnStopBodyTracking.clicked.connect(self.stop_body_tracking)

        self._widget.btnStartHeadTracking.clicked.connect(self.start_head_tracking)
        self._widget.btnStopHeadTracking.clicked.connect(self.stop_head_tracking)


        self._widget.btnStartSimulationController.clicked.connect(self.start_simulation_controller)
        self._widget.btnStopSimulationController.clicked.connect(self.stop_simulation_controller)

        self._widget.btnEnableJoints.clicked.connect(self.enable_joints)
        self._widget.btnDisableJoints.clicked.connect(self.disable_joints)

        self.head_tracker_pub = rospy.Publisher('/head_pose_estimation/event_in', String, queue_size=1)

        self.body_tracker_pub = rospy.Publisher('/tracker_joint_controller/event_in', String, queue_size=1)

        self.simulation_controller_pub = rospy.Publisher('/zeno_robokind_interface_ros_node/event_in', String, queue_size=1)

        self.activate_joints_pub = rospy.Publisher('/zeno_robokind_interface_ros_node/activate_joints', Bool, queue_size=1)

        self._widget.setObjectName('ZENOSkeletonTrackerUIPlugin')

        # Show _widget.windowTitle on left-top of each plugin (when 
        # it's set in _widget). This is useful when you open multiple 
        # plugins at once. Also if you open multiple instances of your 
        # plugin at once, these lines add number to make it easy to 
        # tell from pane to pane.
        if context.serial_number() > 1:
            self._widget.setWindowTitle(self._widget.windowTitle() + (' (%d)' % context.serial_number()))
        # Add widget to the user interface
        context.add_widget(self._widget)

    def init(self):
        print("init")

    def start_skeleton_tracking(self):
        print("start_skeleton_tracking")
        self.body_tracker_pub.publish('e_start')
        self.head_tracker_pub.publish('e_start')

    def stop_skeleton_tracking(self):
        self.body_tracker_pub.publish('e_stop')
        self.head_tracker_pub.publish('e_stop')

    def start_body_tracking(self):
        self.body_tracker_pub.publish('e_start')

    def stop_body_tracking(self):
        self.body_tracker_pub.publish('e_stop')

    def start_head_tracking(self):
        self.head_tracker_pub.publish('e_start')

    def stop_head_tracking(self):
        self.head_tracker_pub.publish('e_stop')

    def start_simulation_controller(self):
        self.simulation_controller_pub.publish('e_start')

    def stop_simulation_controller(self):
        self.simulation_controller_pub.publish('e_stop')

    def enable_joints(self):
        self.activate_joints_pub.publish(True)

    def disable_joints(self):
        self.activate_joints_pub.publish(False)

    def shutdown_plugin(self):
        # TODO unregister all publishers here
        pass

    def save_settings(self, plugin_settings, instance_settings):
        # TODO save intrinsic configuration, usually using:
        # instance_settings.set_value(k, v)
        pass

    def restore_settings(self, plugin_settings, instance_settings):
        # TODO restore intrinsic configuration, usually using:
        # v = instance_settings.value(k)
        pass

    #def trigger_configuration(self):
        # Comment in to signal that the plugin has a way to configure
        # This will enable a setting button (gear icon) in each dock widget title bar
        # Usually used to open a modal configuration dialog
        
